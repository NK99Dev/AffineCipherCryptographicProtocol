package nk.omsu

import java.io._

/**
  * Created by nk16 on 06.09.16.
  */

object Main {
  def gcd (a:Long, b:Long):Long =
    if (b == 0)
      a
    else
      gcd (b, a % b)
  def extended_euclid( a: Long,  b:Long):Long ={
    var  q, r, x1, x2, y1, y2, x, d, y: Long = 0
    var a2 = a
    var b2 = b
    if (b == 0) {
      d = a2
      x = 1
      y = 0
    }

    x2 = 1; x1 = 0; y2 = 0; y1 = 1

    while (b2 > 0) {
      q = a2 / b2
      r = a2 - q * b2
      x = x2 - q * x1
      y = y2 - q * y1
      a2 = b2
      b2 = r
      x2 = x1; x1 = x; y2 = y1; y1 = y
    }

    x2
  }
  def readBytes(file:File):Array[Byte] = {

    val is = new FileInputStream(file)
    val len = file.length.toInt
    val bytesInp = new Array[Byte](len)
    var offset = 0
    var numRead = is.read(bytesInp, offset, len - offset)

    while(offset < len && numRead == 0) {
      offset += numRead
      numRead = is.read(bytesInp, offset, len - offset)
    }
    is.close()
    bytesInp
  }//
  def printStr(fileName: String, array: Array[Byte]): Unit ={
    val out = new FileOutputStream(new File(fileName))
    out.write(array)
    out.close()
  }
  def encryption(a: Long, b: Long, m: Long, fileNameE:String):Unit = {

    val bytesInp = readBytes(new File(fileNameE))
    var bytesOut = new Array[Byte](bytesInp.length)

    for (i <- bytesInp.indices) {
      var M = bytesInp(i)
      var E = a * M + b % m
      bytesOut(i) = E.toByte
    }
    val str = new String(bytesInp, "UTF-8")
    System.out.println("Inp: " + str)
    System.out.println("EncrypteBytes: " + bytesOut)
    printStr(fileNameE, bytesOut)
  }
  def decryption(a: Long, b: Long, m: Long, fileNameD:String):Unit ={

    val bytesInp = readBytes(new File(fileNameD))
    var bytesOut = new Array[Byte](bytesInp.length)

    for (i <- bytesInp.indices){
      var C = bytesInp(i)
      var s: Long = 0

      if (gcd(a, m) == 1)
        s = extended_euclid(a, m)

      var D =  s*((C-b) % m)
      bytesOut(i) = D.toByte
    }
    printStr(fileNameD, bytesOut)
  }
  def main(args:Array[String]) :Unit ={
    var a: Long = 19
    var b: Long = 56
    var m: Long = 256
    val filenameE = readLine("Введите имя исходного файла:")//"/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/Lab1(scala, sbt)/src/main/resources/f.txt"

    encryption(a, b, m, filenameE)

    decryption(a, b, m, filenameE)
  }
}